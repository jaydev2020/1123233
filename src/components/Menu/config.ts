import { MenuEntry } from '@pizzafinance/ui-sdk'

const config: MenuEntry[] = [
  {
    label: 'Home',
    icon: 'HomeIcon',
    href: '/',
  },
  {
    label: 'Trade',
    icon: 'TradeIcon',
    items: [
      {
        label: 'Exchange',
        href: 'https://exchange.breadswap.finance',
      },
      {
        label: 'Liquidity',
        href: 'https://exchange.breadswap.finance/#/pool',
      },
    ],
  },
  {
    label: 'Farms',
    icon: 'FarmIcon',
    href: '/farms',
  },
  {
    label: 'Pools',
    icon: 'PoolIcon',
    href: '/pools',
  },
  {
    label: 'Lottery',
    icon: 'TicketIcon',
    href: '/lottery',
  },
  {
    label: 'Info',
    icon: 'InfoIcon',
    items: [
      {
        label: 'Overview',
        href: 'https://breadswap.info',
      },
      {
        label: 'Tokens',
        href: 'https://breadswap.info/tokens',
      },
      {
        label: 'Pairs',
        href: 'https://breadswap.info/pairs',
      },
      {
        label: 'Accounts',
        href: 'https://breadswap.info/accounts',
      },
    ],
  },
  {
  label: 'IFO',
  icon: 'IfoIcon',
  href: '/ifo',
  },
  {
    label: 'More',
    icon: 'MoreIcon',
    items: [
      {
        label: 'Telegram Contact',
        href: 'https://t.me/breadswap_contact',
      },
    ],
  },
]

export default config
