import contracts from './contracts'
import { FarmConfig, QuoteToken } from './types'

const farms: FarmConfig[] = [
  {
    pid: 0,
    lpSymbol: 'BREAD',
    lpAddresses: {
      97: '',
      56: '0xA527a61703D82139F8a06Bc30097cC9CAA2df5A6',
    },
    tokenSymbol: 'PASTA',
    tokenAddresses: {
      97: '',
      56: '0x40edad8E997A2238A8b54a6D8543B78FA9CbA627',
    },
    quoteTokenSymbol: QuoteToken.BNB,
    quoteTokenAdresses: contracts.wbnb,
  },
  {
    pid: 1,
    lpSymbol: 'BREAD-BNB LP',
    lpAddresses: {
      97: '',
      56: '0xA527a61703D82139F8a06Bc30097cC9CAA2df5A6',
    },
    tokenSymbol: 'LOBREADNG',
    tokenAddresses: {
      97: '',
      56: '0xBA2fbA507fF19260E5AB1a30c00D284901F1E7F3',
    },
    quoteTokenSymbol: QuoteToken.BNB,
    quoteTokenAdresses: contracts.wbnb,
  },
  {
    pid: 2,
    lpSymbol: 'BREAD-BUSD LP',
    lpAddresses: {
      97: '',
      56: '0xA527a61703D82139F8a06Bc30097cC9CAA2df5A6',
    },
    tokenSymbol: 'BREAD',
    tokenAddresses: {
      97: '',
      56: '0xBA2fbA507fF19260E5AB1a30c00D284901F1E7F3',
    },
    quoteTokenSymbol: QuoteToken.BUSD,
    quoteTokenAdresses: contracts.busd,
    isCommunity: false,
  },
  {
   pid: 11,
   lpSymbol: 'BREAD-USDT LP',
   lpAddresses: {
     97: '',
     56: '0xA527a61703D82139F8a06Bc30097cC9CAA2df5A6',
   },
   tokenSymbol: 'BREAD',
   tokenAddresses: {
     97: '',
     56: '0xBA2fbA507fF19260E5AB1a30c00D284901F1E7F3',
   },
   quoteTokenSymbol: QuoteToken.USDT,
   quoteTokenAdresses: contracts.usdt,
   isCommunity: false,
 },
 {
  pid: 13,
  lpSymbol: 'BREAD-USDC LP',
  lpAddresses: {
    97: '',
    56: '0xA527a61703D82139F8a06Bc30097cC9CAA2df5A6',
  },
  tokenSymbol: 'BREAD',
  tokenAddresses: {
    97: '',
    56: '0xBA2fbA507fF19260E5AB1a30c00D284901F1E7F3',
  },
  quoteTokenSymbol: QuoteToken.USDC,
  quoteTokenAdresses: contracts.usdc,
  isCommunity: false,
},
{
 pid: 14,
 lpSymbol: 'BREAD-DAI LP',
 lpAddresses: {
   97: '',
   56: '0xA527a61703D82139F8a06Bc30097cC9CAA2df5A6',
 },
 tokenSymbol: 'BREAD',
 tokenAddresses: {
   97: '',
   56: '0xBA2fbA507fF19260E5AB1a30c00D284901F1E7F3',
 },
 quoteTokenSymbol: QuoteToken.DAI,
 quoteTokenAdresses: contracts.dai,
 isCommunity: false,
},
  {
    pid: 3,
    lpSymbol: 'BREAD-CHS LP',
    lpAddresses: {
      97: '',
      56: '0xA527a61703D82139F8a06Bc30097cC9CAA2df5A6',
    },
    tokenSymbol: 'BREAD',
    tokenAddresses: {
      97: '',
      56: '0xBA2fbA507fF19260E5AB1a30c00D284901F1E7F3',
    },
    quoteTokenSymbol: QuoteToken.CHS,
    quoteTokenAdresses: contracts.chs,
  },
  {
    pid: 18,
    lpSymbol: 'CAKE-BREAD LP',
    lpAddresses: {
      97: '',
      56: '0xA527a61703D82139F8a06Bc30097cC9CAA2df5A6',
    },
    tokenSymbol: 'BREAD',
    tokenAddresses: {
      97: '',
      56: '0x0e09fabb73bd3ade0a17ecc321fd13a19e81ce82',
    },
    quoteTokenSymbol: QuoteToken.BNB,
    quoteTokenAdresses: contracts.wbnb,
    isCommunity: false,
  },
  {

   pid: 5,
   lpSymbol: 'BNB-BUSD LP',
   lpAddresses: {
     97: '',
     56: '0xA527a61703D82139F8a06Bc30097cC9CAA2df5A6',
   },
   tokenSymbol: 'BNB',
   tokenAddresses: {
     97: '',
     56: '0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c',
   },
   quoteTokenSymbol: QuoteToken.BUSD,
   quoteTokenAdresses: contracts.busd,
   isCommunity: false,
 },
  {
    pid: 7,
    lpSymbol: 'ETH-BUSD LP',
    lpAddresses: {
      97: '',
      56: '0xA527a61703D82139F8a06Bc30097cC9CAA2df5A6',
    },
    tokenSymbol: 'ETH',
    tokenAddresses: {
      97: '',
      56: '0x2170Ed0880ac9A755fd29B2688956BD959F933F8',
    },
    quoteTokenSymbol: QuoteToken.BUSD,
    quoteTokenAdresses: contracts.busd,
  },
 {
   pid: 6,
   lpSymbol: 'USDT-BNB LP',
   lpAddresses: {
     97: '',
     56: '0xA527a61703D82139F8a06Bc30097cC9CAA2df5A6',
   },
   tokenSymbol: 'USDT',
   tokenAddresses: {
     97: '',
     56: '0x55d398326f99059fF775485246999027B3197955',
   },
   quoteTokenSymbol: QuoteToken.BNB,
   quoteTokenAdresses: contracts.wbnb,
   isCommunity: false,
 },
 {
  pid: 8,
  lpSymbol: 'DAI-BNB LP',
  lpAddresses: {
    97: '',
    56: '0xA527a61703D82139F8a06Bc30097cC9CAA2df5A6',
  },
  tokenSymbol: 'DAI',
  tokenAddresses: {
    97: '',
    56: '0x1AF3F329e8BE154074D8769D1FFa4eE058B1DBc3',
  },
  quoteTokenSymbol: QuoteToken.BNB,
  quoteTokenAdresses: contracts.wbnb,
  isCommunity: false,
},
{
 pid: 9,
 lpSymbol: 'USDC-BNB LP',
 lpAddresses: {
   97: '',
   56: '0xA527a61703D82139F8a06Bc30097cC9CAA2df5A6',
 },
 tokenSymbol: 'USDC',
 tokenAddresses: {
   97: '',
   56: '0x8AC76a51cc950d9822D68b83fE1Ad97B32Cd580d',
 },
 quoteTokenSymbol: QuoteToken.BNB,
 quoteTokenAdresses: contracts.wbnb,
 isCommunity: false,

  },
]

export default farms
